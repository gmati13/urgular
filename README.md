# Urgular Framework

Framework based on Urwid and Angular

## Usage

### Installation
```sh
pip install urwid cssutils
pip install -i https://test.pypi.org/simple/ urgular
```

### Example

Files:
```text
test.py
test.xml
main.py
```

test.xml
```xml
<frame>
    <list-box role="body">
        <text>{{self.test}}</text>
        <text>press Ctrl-C to exit</text>
    </list-box>
</frame>
```

test.py
```python
import os
from urgular.core import Component

@Component(
    'test',
    os.path.abspath('test.xml')
)
class Test:
    def __init__(self):
        self.test = 'hello world'
```

main.py
```python
from urgular.core import urgular
from test import Test

urgular.define_components('app', root=Test)
urgular.start()
```

Run `python main.py`

---

## Contributing

requirements:
```sh
python v3.7
```

### virtualenv configuration:

create virtualenv:

```sh
python3.7 -m venv venv
```

activate virtualenv:
```sh
source venv/bin/activate
```

deactivate virtualenv:
```sh
deactivate
```

### start application

```sh
python main.py
```

### dependencies

install dependencies:

```sh
pip install -r requirements.txt
```

generate or update requirements.txt:

```sh
pip freeze > requirements.txt
```
