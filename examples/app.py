from urgular.component import component
from urgular.core import Urgular
from os import path
from rx.subject import BehaviorSubject


@component('app', path.join(path.dirname(__file__), 'app.xml'))
class App:
    def __init__(self):
        self.table = [
            {
                'head': 'id',
                'items': [0, 1, 2, 3]
            },
            {
                'head': 'name',
                'items': ['Mati', 'Boris', 'Veronica', 'Ivan']
            },
            {
                'head': 'nickname',
                'items': ['gmati13', 'zavgo', 'nico', 'games_ranger']
            }
        ]


@component('child', path.join(path.dirname(__file__), 'child.xml'))
class Child:
    def __init__(self):
        self.test = BehaviorSubject('hello world')
        self.test_property = lambda: ''


urgular = Urgular(
    'vigram',
    App,
    Child
)

urgular.render()
