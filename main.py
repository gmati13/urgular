import urwid
from urgular.core import urgular, Component, Service, queue
import os


@Service()
class TestService:
    def __init__(self):
        self.text = 'initial text'

    def set_text(self, text):
        self.text = text

    def keypress(self, edit: urwid.Edit, original, *args):
        result = original(*args)
        self.set_text(edit.get_edit_text())
        return result


@Component(
    'app',
    os.path.abspath('examples/app.xml'),
    [os.path.abspath('examples/app.css')],
    inject=[TestService]
)
class App:
    def __init__(self):
        self.testService: TestService


@Component(
    'test',
    os.path.abspath('examples/child.xml'),
    inject=[TestService]
)
class Test:
    def __init__(self):
        self.testService: TestService

urgular.enable_logging()
urgular.define_services([TestService])
urgular.define_components(
    'app',
    root=App,
    components=[Test]
)
urgular.start()
