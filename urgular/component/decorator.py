from urgular.tools.xml_reader import XMLReader


reader = XMLReader()


def component(component_tag_name, xml_template_path):
    if not component_tag_name or not isinstance(component_tag_name, str):
        raise TypeError('Incorrect component tag name: {} should be a string'.format(component_tag_name))
    if not xml_template_path or not isinstance(xml_template_path, str):
        raise TypeError('Incorrect xml template path: {} should be a string'.format(component_tag_name))

    def decorator(component_class):
        component_class.tree = reader.parse(xml_template_path)
        component_class.tag_name = component_tag_name

        return component_class

    return decorator
