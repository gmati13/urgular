from typing import List
import xml.sax


class XMLElement:
    def __init__(
            self,
            tag,
            attributes: dict or None = None,
            parent: 'XMLElement' or None = None,
            content=None,
            children: List['XMLElement'] or None = None
    ):
        self.tag = tag
        self.attributes = attributes
        self.parent = parent
        self.content = content
        self.children = children

    def copy(self):
        return XMLElement(
            self.tag,
            self.attributes.copy(),
            self.parent,
            self.content,
            self.children
        )


class XMLReader(xml.sax.handler.ContentHandler):
    def __init__(self, on_element_end=None):
        super().__init__()
        self.parser = xml.sax.make_parser()
        self.parser.setContentHandler(self)
        self.data: List[XMLElement] = []
        self.current_element: XMLElement or None = None
        self.level = None
        self.on_element_end = on_element_end

    def parse(self, filename):
        self.parser.parse(filename)
        return self.data

    def startDocument(self):
        self.level = 0
        self.data = []

    def startElement(self, tag, attributes):
        attrs = {key: attributes.get(key) for key in attributes.keys()}

        new_element = XMLElement(
            tag=tag,
            attributes=attrs if attrs.__len__() > 0 else None,
            parent=self.current_element
        )

        if self.level == 0:
            self.data.append(new_element)
        else:
            if self.current_element is None:
                self.data.append(new_element)
            else:
                if self.current_element.children is None:
                    self.current_element.children = []
                self.current_element.children.append(new_element)

        self.current_element = new_element
        self.level += 1

    def endElement(self, tag):
        self.level -= 1
        if callable(self.on_element_end):
            self.on_element_end(self.current_element, self.level + 1)
        self.current_element = self.current_element.parent

    def characters(self, content):
        if self.current_element is None:
            return

        self.current_element.content =\
            (
                content
                if self.current_element.content is None
                else self.current_element.content + '\n' + content
            )\
            if content is not None and content != ''\
            else self.current_element.content
