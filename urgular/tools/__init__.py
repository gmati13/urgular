from .xml_reader import XMLReader


def eval_code(self, __code__, __scope__):
    for __key__ in __scope__:
        locals()[__key__] = __scope__[__key__]
    return eval(__code__)
