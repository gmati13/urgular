from typing import List, Dict
import urwid
from urgular.tools.xml_reader import XMLReader, XMLElement
from urgular.tools import eval_code
import re

_patterns = {
    'segment': re.compile(r'{{.*?}}'),
    'code': re.compile(r'{{|}}'),
    'for': re.compile(r'^(.*?) in (.*?)$'),
}


class TreeNode:
    def __init__(self, parent_component, xml: XMLElement, proto, meta, children: List['TreeNode'], scope, urgular):
        import inspect
        import uuid
        import re

        self.xml: XMLElement = xml
        self.meta = meta
        self.children = children
        self.parent: TreeNode or None = None
        self.native: urwid.Widget or None = None
        self.is_dynamic = False
        self.dynamic_children: List[TreeNode] = []
        self.cached_nodes: List[TreeNode] = []
        self.uuid = uuid.uuid1()
        self.scope = scope
        self.has_dynamic_children = False
        self.segment_positions = []
        self.segments: List[str] = []
        self.cache = {}
        for match in _patterns['segment'].finditer(xml.content if xml.content else ''):
            self.segment_positions.insert(0, match.span())
            self.segments.insert(0, match.group())
        self.segments_code: Dict[str] = {}
        self.segment_cache_keys: Dict[str] = {}

        self.attributes: List[str] = []
        self.attributes_code: Dict[str] = {}
        self.attribute_cache_keys: Dict[str] = {}

        self.methods = []
        self.methods_code = {}
        self.methods_cache_keys = {}

        self.is_custom = False
        self.proto = proto
        self.parent_component = None
        self.for_pair = None, None
        self.if_statement_code = None

        for segment in self.segments:
            self.segments_code[segment] = compile(re.sub(_patterns['code'], '', segment), '<string>', 'eval')
            self.segment_cache_keys[segment] = segment

        tmp_dynamic_children = []
        for child in children:
            if not child.is_dynamic:
                tmp_dynamic_children.append(child)
            else:
                self.has_dynamic_children = True

        if self.has_dynamic_children:
            self.dynamic_children = tmp_dynamic_children

        supported_attributes = proto.get_supported_attributes() \
            if hasattr(proto, 'urgular') \
            else inspect.signature(proto).parameters

        for attribute in (xml.attributes if xml.attributes else []):
            if attribute == 'class':
                continue
            elif attribute in ['if', 'for']:
                self.is_dynamic = True
                if attribute == 'for':
                    name, for_statement = re.match(_patterns['for'], xml.attributes['for']).groups()
                    self.for_pair = name, compile(for_statement, '<string>', 'eval')
                elif attribute == 'if':
                    self.if_statement_code = compile(xml.attributes['if'], '<string>', 'eval')
            elif attribute in supported_attributes:
                self.attributes.append(attribute)
                self.attributes_code[attribute] = compile(xml.attributes[attribute], '<string>', 'eval')
                self.attribute_cache_keys[attribute] = attribute
                try:
                    meta[attribute] = eval_code(
                        parent_component,
                        compile(xml.attributes[attribute], '<string>', 'eval'),
                        self.parent.scope if self.parent else self.scope
                    )
                except NameError:
                    meta[attribute] = ''
            elif hasattr(proto, attribute):
                self.methods.append(attribute)
                self.methods_code[attribute] = compile(xml.attributes[attribute], '<string>', 'eval')
                self.methods_cache_keys[attribute] = attribute

        if hasattr(proto, 'urgular'):
            self.is_custom = True
            self.parent_component = proto.render(**meta)
            self.target = self.parent_component.node.target
        else:
            self.target = proto(**meta)

        self.parent_component = parent_component
        self.native = self.target
        object.__setattr__(self.target, '__tree_node', self)
        object.__setattr__(self.target, '__parent_component', parent_component)

        if xml.attributes and 'class' in xml.attributes:
            selector = '.' + xml.attributes['class']
            focus_selector = selector + ':focus'
            if selector not in urgular.styles:
                return
            styles = urgular.styles[selector]
            if 'color' in styles or 'background' in styles:
                self.native = urwid.AttrMap(
                    self.native,
                    attr_map=selector,
                    focus_map=focus_selector if focus_selector in urgular.styles else None
                )


class Tree:
    def __init__(self, urgular):
        self.reader = XMLReader(self.on_element_read)
        self.urgular = urgular
        self.root: TreeNode or None = None
        self.temporary_children: Dict[List[TreeNode]] = {}
        self.component_instance = None
        self.scope = {}

    def read_xml(self, element: XMLElement, level=0):
        for child in (element.children if element.children else []):
            self.read_xml(child, level + 1)
        self.on_element_read(element, level)

    def on_element_read(self, element: XMLElement, level):
        import re

        rule_tuple = (None, [])
        if element.tag in self.urgular.content_rules:
            rule_tuple = self.urgular.content_rules[element.tag]

        content_type, rules = rule_tuple
        named_arguments = {}

        all_tags = []
        required_tags = []
        for tag, required in rules:
            all_tags.append(tag)
            if required:
                required_tags.append(tag)

        if content_type == 'text_content':
            for tag_name in all_tags:
                named_arguments[tag_name] = re.sub(r'{{.*?}}', '', element.content if element.content else '')
        elif content_type == 'attribute_specified':
            for child in self.temporary_children[level]:
                if 'role' not in (child.xml.attributes if child.xml.attributes else []):
                    raise AttributeError('Missing "role" attribute for {} as child of {}'.format(
                        child.xml.tag,
                        element.tag
                    ))
                if child.xml.attributes['role'] in named_arguments:
                    raise AttributeError('Duplicate attribute specified tag {} for {}'.format(
                        child.xml.tag,
                        element.tag
                    ))
                named_arguments[child.xml.attributes['role']] = child.native
        elif content_type == 'native_array':
            named_arguments[required_tags[0]] = [
                child.native for child in self.temporary_children[level] if child.native
            ]
        elif content_type == 'native':
            named_arguments[required_tags[0]] = self.temporary_children[level][0].native
        else:
            named_arguments['widget_list'] = []
            if element.children is not None:
                named_arguments['widget_list'] = [
                    child.native for child in self.temporary_children[level] if child.native
                ]

        for required_tag in required_tags:
            if required_tag not in named_arguments:
                raise AttributeError(('Missing required element with role="{}" for {}'.format(
                    required_tag,
                    element.tag
                )))

        tree_node = TreeNode(
            self.component_instance,
            element,
            self.urgular.components_dictionary[element.tag],
            named_arguments,
            self.temporary_children[level] if level in self.temporary_children else [],
            self.scope,
            self.urgular
        )

        if level in self.temporary_children:
            for child in self.temporary_children[level]:
                child.parent = tree_node
            self.temporary_children[level] = []
        if (level - 1) not in self.temporary_children:
            self.temporary_children[level - 1] = []
        self.temporary_children[level - 1].append(tree_node)

        if element.parent is None:
            self.root = tree_node

    def read_file(self, path_to_xml_file):
        self.reader.parse(path_to_xml_file)
        self.temporary_children = None
