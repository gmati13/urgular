from typing import List


class QueueTask:
    def __init__(self, callback, parent_queue, unbreakable=False):
        import string
        import random
        import time

        self.__callback = callback
        self.id = '{}#{}'.format(
            ''.join(random.choice(string.ascii_letters) for i in range(10)),
            time.time()
        )
        self.queue = parent_queue
        self.unbreakable = unbreakable

    def cancel(self):
        self.queue.cancel(self)

    def start(self):
        self.__callback()

    def next_tick(self):
        self.queue.next_tick(self)


class Queue:
    def __init__(self):
        self.__tasks: List[QueueTask] = []
        self.__next: List[QueueTask] = []
        self.__alive = False
        self.__thread = None
        self.__fps = 100
        self.__tick = 1 / self.__fps
        pass

    def __loop(self):
        import time
        self.__alive = True
        previous = time.time()

        while True:
            if not self.__alive:
                break

            difference = time.time() - previous
            if difference < self.__tick:
                time.sleep(self.__tick - difference)
            previous = time.time()

            self.__tasks = self.__next
            self.__next = []

            for task in self.__tasks:
                task.start()

    def next_tick(self, callback):
        if isinstance(callback, QueueTask):
            task = callback
        else:
            task = self.create_task(callback)
        self.__next.append(task)

        return task

    def start(self):
        import threading
        self.__thread = threading.Thread(target=self.__loop)
        self.__thread.start()

    def kill(self):
        self.__alive = False

    def cancel(self, task_to_cancel: QueueTask):
        self.kill()
        for index, task in enumerate(self.__next):
            if task.id == task_to_cancel.id:
                self.__next.pop(index)
                break
        self.start()

    def create_task(self, callback, unbreakable=False) -> QueueTask:
        if not callable(callback):
            raise ValueError('Queue.create_task: {} should be callable'.format(str(callback)))

        return QueueTask(callback, self, unbreakable)

    def set_timeout(self, callback, delay) -> QueueTask:
        import time

        if not callable(callback):
            raise ValueError('Queue.set_timeout: {} should be callable'.format(str(callback)))

        start = time.time()
        task = None

        def timeout():
            nonlocal start, self, task
            if time.time() - start < delay:
                self.next_tick(task)
                return
            callback()

        task = self.next_tick(timeout)
        return task

    def set_interval(self, callback, delay) -> QueueTask:
        import time

        if not callable(callback):
            raise ValueError('Queue.set_interval: {} should be callable'.format(str(callback)))

        start = time.time()
        task = None

        def interval():
            nonlocal start, self, task
            if time.time() - start >= delay:
                start = time.time()
                callback()
            self.next_tick(task)

        task = self.next_tick(interval)
        return task


queue = Queue()
