import urwid
from typing import List
from urgular.core.urgular import map_urgular
from urgular.core.tree import Tree, TreeNode
from urgular.tools import eval_code
import cssutils

cssutils.log.enabled = False


@map_urgular
class Component:
    def __init__(self, tag_name, xml_template, styles: List[str] or None = None, rule=None, inject=None):
        self.tag_name = tag_name
        self.xml_template = xml_template
        self.constructor = None
        self.instances = []
        self.rule = rule
        self.style_rules: cssutils.css.CSSRuleList or [] = []
        self.inject = inject
        for style in (styles if styles else []):
            self.style_rules = cssutils.parseFile(style).cssRules

    def __call__(self, constructor):
        self.constructor = constructor
        self.constructor.node = None
        self.constructor.urgular = None
        self.constructor.factory = None
        return self

    def get_supported_attributes(self):
        import inspect
        return inspect.signature(self.constructor).parameters

    def render(self, **meta):
        supported_attributes = self.get_supported_attributes()
        attributes = {key: meta[key] for key in meta if key in supported_attributes}

        for constructor in (self.inject if self.inject else []):
            setattr(
                self.constructor,
                constructor.service_name,
                self.urgular.services[constructor]
            )

        instance = self.constructor(**attributes)
        instance.factory = self
        instance.urgular = self.urgular
        tree = Tree(self.urgular)
        tree.component_instance = instance
        tree.reader.parse(self.xml_template)
        instance.node = tree.root
        self.instances.append(instance)
        return instance

    def updater(self):
        updated = False

        for instance in self.instances:
            updated = self.process_dynamic_nodes(instance.node, instance) or updated
            # self.process_segments_for(instance.node, instance)
        return updated

    def process_dynamic_nodes(self, node: TreeNode, instance):
        children = []
        something_changed = False

        for child in node.children:
            if not child.is_dynamic:
                child.scope = node.scope.copy()
                children.append(child)
                self.process_dynamic_nodes(child, instance)
                continue

            result = []

            has_if_statement = 'if' in child.xml.attributes
            is_generator = 'for' in child.xml.attributes

            if_statement = False

            if has_if_statement:
                cached = child.cache['if'] if 'if' in child.cache else None
                if_statement = eval_code(instance, child.if_statement_code, node.scope)

                if str(if_statement) != cached:
                    something_changed = True

                child.cache['if'] = str(if_statement)

                if not if_statement:
                    continue

            name, target = child.for_pair

            cached = child.cache['for'] if 'for' in child.cache else None

            now = if_statement

            if is_generator:
                now = eval_code(instance, target, node.scope)
                if str(now) == cached and not has_if_statement:
                    continue
                child.cache['for'] = str(now)
            elif has_if_statement and not something_changed:
                continue

            for index, item in enumerate(now if is_generator else [now]):
                if index < child.cached_nodes.__len__():
                    dynamic_node = child.cached_nodes[index]
                else:
                    tree = Tree(self.urgular)
                    tree.component_instance = instance
                    tree.scope = node.scope.copy()
                    if is_generator:
                        tree.scope[name] = item
                    element = child.xml.copy()
                    element.parent = None
                    if is_generator:
                        element.attributes.pop('for')
                    if has_if_statement:
                        element.attributes.pop('if')
                    tree.read_xml(element)
                    tree.root.parent = node
                    dynamic_node = tree.root
                dynamic_node.scope = node.scope.copy()
                if is_generator:
                    dynamic_node.scope[name] = item
                self.process_dynamic_nodes(dynamic_node, instance)
                result.append(dynamic_node)

            child.cached_nodes = result

            something_changed = True
            children += result

        if not something_changed:
            return False

        for child in node.dynamic_children:
            if child not in children:
                if 'for' in child.cache:
                    del child.cache['for']
                for attribute in child.attributes:
                    del child.cache[attribute]
                for segment in child.segments:
                    del child.cache[segment]

        node.dynamic_children = children

        content_name = 'body' if node.xml.tag == 'list-box' else 'contents'
        content = getattr(node.target, content_name)
        focus_position = node.target.focus_position
        content.clear()
        if node.xml.tag == 'columns':
            content.extend([(child.native, node.target.options()) for child in children])
        else:
            content.extend([child.native for child in children])
        if content.__len__() > 0:
            node.target.focus_position = focus_position if focus_position < content.__len__() else content.__len__() - 1

        return something_changed
