from .service import Service

@Service()
class LoggerService:
    def __init__(self):
        self.logs = []

    def log(self, *msg):
        self.logs.insert(0, msg)