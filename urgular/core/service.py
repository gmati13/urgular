from urgular.core.urgular import map_urgular


@map_urgular
class Service:
    def __init__(self):
        self.service_name = None
        self.instance = None

    def __call__(self, constructor):
        self.constructor = constructor
        self.service_name = constructor.__name__[0].lower() + constructor.__name__[1:]
        return self

    def inject(self):
        import inspect
        self.constructor.__service_name = None
        self.constructor.urgular = self.urgular

        parameters = {}

        for service_name in inspect.signature(self.constructor).parameters:
            parameters[service_name] = self.urgular.services[service_name]

        self.instance = self.constructor(**parameters)
        return self.instance
