import os
from .component import Component
from .logger_service import LoggerService

@Component(
    'urgular-logger',
    os.path.dirname(__file__) + '/logger.xml',
    inject=[LoggerService]
)
class Logger:
    pass
