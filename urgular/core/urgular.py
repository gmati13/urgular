import urwid
from typing import List
from .queue import queue
from urgular.tools import eval_code
import cssutils


class Urgular:
    def __init__(self):
        self.components_dictionary = {
            'frame': urwid.Frame,
            'columns': urwid.Columns,
            'text': urwid.Text,
            'list-box': urwid.ListBox,
            'pile': urwid.Pile,
            'edit': urwid.Edit,
            'box-adapter': urwid.BoxAdapter,
            'filler': urwid.Filler,
            'padding': urwid.Padding
        }

        self.content_rules = {
            'frame': ('attribute_specified', [
                ('header', False),
                ('body', True),
                ('footer', False)
            ]),
            'text': ('text_content', [('markup', True)]),
            'edit': ('text_content', [('caption', True)]),
            'list-box': ('native_array', [('body', True)]),
            'box-adapter': ('native', [('box_widget', True)]),
            'filler': ('native', [('body', True)]),
            'padding': ('native', [('w', True)])
        }

        self.root = None
        self.components = []
        self.running = False
        self.fps = 0
        self.app: urwid.MainLoop or None = None
        self.palette = []
        self.styles = {}
        self.allow_to_exit = False
        self.logger = None

        self.services = {}
        self.updater_locked = False
        self.rendered_elements = []

    def process_segments_for(self, node, instance):
        updated = False

        content_type, rules = self.content_rules[node.xml.tag] \
            if node.xml.tag in self.content_rules \
            else (None, None)

        for attribute in node.attributes_code:
            code = node.attributes_code[attribute]
            cached = node.cache[attribute] if attribute in node.cache else None

            now = eval_code(instance, code, node.scope.copy())
            if now == cached:
                continue
            updated = True
            node.cache[attribute] = now
            if node.is_custom:
                object.__setattr__(node.parent_component, attribute, now)
            else:
                if attribute == 'keypress':
                    exit()
                if isinstance(node.native, urwid.Edit) and attribute == 'caption':
                    node.native.set_caption(now)
                else:
                    object.__setattr__(node.native, attribute, now)

        for method in node.methods_code:
            code = node.methods_code[method]
            cached = node.cache[method] if method in node.cache else None

            now = eval_code(instance, code, node.scope.copy())
            if now == cached:
                continue
            updated = True
            node.cache[method] = now
            if node.is_custom:
                object.__setattr__(node.parent_component, method, now)
            else:
                original_method = object.__getattribute__(node.native, method)

                def wrapper_method(*args, **kwargs):
                    return now(node.native, original_method, *args, **kwargs)

                object.__setattr__(node.native, method, wrapper_method)

        if content_type != 'text_content':
            return

        something_changed = False
        result = node.xml.content if node.xml.content else ''

        for segment in node.segments_code:
            code = node.segments_code[segment]
            cached = node.cache[segment] if segment in node.cache else None

            now = eval_code(instance, code, node.scope.copy())
            if now == cached:
                continue
            updated = True
            something_changed = True
            node.cache[segment] = now

        if something_changed is False:
            return updated

        for index, segment in enumerate(node.segments):
            start, end = node.segment_positions[index]
            result = result[:start] + (str(node.cache[segment]) if segment in node.cache else '') + result[end:]

        if node.is_custom and rules is not None:
            prop_name, _ = rules[0]
            object.__setattr__(node.parent_component, prop_name, result)
        elif isinstance(node.target, urwid.Text):
            node.target.set_text(result)
        elif isinstance(node.target, urwid.Edit):
            node.target.set_caption(result)

        return updated

    def update_rendered(self):
        updated = False
        completed_factories = []

        for parent_component, node in self.rendered_elements:
            if parent_component not in completed_factories:
                updated = parent_component.factory.updater() or updated
                completed_factories.append(parent_component)

            updated = self.process_segments_for(node, parent_component) or updated

        self.rendered_elements = []
        return updated

    def on_render_root_widget(self, canvas: urwid.CompositeCanvas):
        def recursive(children):
            nonlocal self

            for child in children:
                if child[2].widget_info:
                    node = None
                    try:
                        node = object.__getattribute__(child[2].widget_info[0], '__tree_node')
                    except AttributeError:
                        continue

                    (name, _) = node.for_pair
                    if name is not None and name not in node.scope:
                        continue

                    self.rendered_elements.append((
                        object.__getattribute__(child[2].widget_info[0], '__parent_component'),
                        node
                    ))
                if hasattr(child[2], 'children'):
                    recursive(child[2].children)

        if self.allow_to_exit:
            recursive(canvas.children)

    def override_render(self, widget: urwid.Widget):
        original_render = object.__getattribute__(widget, 'render')

        def wrapped_render(*args, **kwargs):
            nonlocal self

            result = original_render(*args, **kwargs)
            if len(self.rendered_elements) == 0:
                self.on_render_root_widget(result)
            return result

        object.__setattr__(widget, 'render', wrapped_render)

    def render(self):
        self.app = urwid.MainLoop(widget=self.root.render().node.native)
        self.override_render(self.app.widget)
        self.app.screen.set_terminal_properties(colors=256)
        self.app.screen.register_palette(self.palette)

    def enable_logging(self):
        from .logger_service import LoggerService
        from .logger import Logger
        self.logger = LoggerService.inject()
        self.services[LoggerService] = self.logger
        self.components_dictionary[Logger.tag_name] = Logger
        self.components.append(Logger)
        self.apply_styles(Logger.tag_name, Logger.style_rules)

    def define_services(self, services=None):
        for service in (services if services else[]):
            self.services[service] = service.inject()

    def define_components(self, prefix, root, components=None):
        self.apply_styles(root.tag_name, root.style_rules)
        root.is_root = True
        self.root = root

        for component in (components if components else []):
            tag_name = '{}-{}'.format(prefix, component.tag_name)
            if tag_name in self.components_dictionary:
                raise ValueError('Duplicate component with the same tag name {}'.format(tag_name))
            self.components_dictionary[tag_name] = component
            self.components.append(component)
            self.apply_styles(component.tag_name, component.style_rules)
            if component.rule:
                self.content_rules[tag_name] = component.rule

        self.render()

    def apply_styles(self, tag, rules: cssutils.css.CSSRuleList):
        def register(css_rule, selector=None):
            a = (
                selector,
                css_rule.style['font-style'] if 'font-style' in css_rule.style else 'default',
                '',
                '',
                css_rule.style['color'] if 'color' in css_rule.style else 'default',
                css_rule.style['background'] if 'background' in css_rule.style else 'default',
            )
            self.palette.append(a)

        for rule in rules:
            register(rule, rule.selectorText)
            if ':focus' not in rule.selectorText:
                self.styles[rule.selectorText] = rule.style
                if (rule.selectorText + ':focus') not in [s for s, *_ in self.palette]:
                    register(rule, rule.selectorText + ':focus')

    def start(self):
        import time

        try:
            queue.start()
            self.app.start()
            self.app.draw_screen()
            self.running = True

            start = time.time()
            count = 0

            def updater():
                nonlocal count, start
                # for component in self.components:
                    # component.updater()
                # self.root.updater()
                self.allow_to_exit = True
                if self.update_rendered() or not count:
                    try:
                        self.app.draw_screen()
                    except (TypeError, AssertionError):
                        pass

                count += 1
                if time.time() - start >= 1:
                    start = time.time()
                    self.fps = count
                    count = 0

            queue.set_interval(updater, 0)
            self.app.event_loop.run()
        except KeyboardInterrupt:
            import sys
            self.stop()
            self.running = False
            print('Interrupted')
            sys.exit(0)

    def stop(self):
        queue.kill()
        self.app.stop()


urgular = Urgular()


def map_urgular(_class):
    _class.urgular = urgular
    return _class
